import pandas as pd
import numpy as np
import os, re, sys, math, random, cPickle, csv, sklearn

from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.svm import SVC, LinearSVC
from sklearn.metrics import classification_report, f1_score, accuracy_score, confusion_matrix
from sklearn.pipeline import Pipeline
from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import StratifiedKFold, cross_val_score, train_test_split
from textblob import TextBlob
from nltk.corpus import stopwords
from email.parser import Parser
from nltk import word_tokenize, WordNetLemmatizer
from string import digits

business_emails_path = 'data/emails/business/'
personal_emails_path = 'data/emails/personal/'
sms_data_path = "data/sms/metadata.csv"
dataset_path = 'dataset/data.csv'
classifier_path = 'classifier/text-classifier.pkl'
report_path = 'report/report.txt'

def extract_email_body(path):
   body_list = []
   for email in os.listdir(path):
      with open(os.path.join(path, email), 'r') as f:
         lines = f.readlines()
         lines = lines[:-1] #remove murkup
         data = ''.join(lines)
         body = Parser().parsestr(data).get_payload().translate(None, digits)
         body_list.append(body)
   return body_list

def normilize_data(text):
    stop_words = set(stopwords.words('english'))
    result = []
    if (type(text) == str):
        text = re.sub(r"http\S+", "", text) #remove links
        text = re.sub(r"\S*@\S*\s?", "", text) #remove email addresses
        text = text.lower() #to lowercase
        words = TextBlob(text).words
        result = [word.lemma for word in words if word not in stop_words and len(word) > 1]
    return result

def train_svm():
    data = pd.read_csv(dataset_path, sep='\t', names=["category", "text"])
    text_train, text_test, category_train, category_test = train_test_split(data['text'], data['category'], test_size=0.2)
    pipeline = Pipeline([
    ('vect', CountVectorizer(analyzer=normilize_data)),
    ('tfidf', TfidfTransformer()),
    ('classifier', SVC())])

    parameters_grid = [
        {'classifier__C': [1, 10, 100, 1000], 'classifier__kernel': ['linear']},
        {'classifier__C': [1, 10, 100, 1000], 'classifier__gamma': [0.001, 0.0001], 'classifier__kernel': ['rbf']},
    ]

    #parameters tuning
    grid = GridSearchCV(
        pipeline,
        param_grid=parameters_grid,
        refit=True,
        n_jobs=1,
        scoring='accuracy',
        cv=StratifiedKFold(category_train, n_folds=5),
    )

    svm_classifier = grid.fit(text_train, category_train)

    if not os.path.exists("report/"):
        os.makedirs("report/")

    if not os.path.exists("classifier/"):
        os.makedirs("classifier/")
    with open(classifier_path, 'wb') as f:
        cPickle.dump(svm_classifier, f)
    f.close()

    with open(report_path, 'w') as f:
        f.write("Best parameters for SVM classifier: ")
        f.write(str(grid.best_params_))
        f.write("\n")
        f.write("Report: \n")
        f.write(classification_report(category_test, svm_classifier.predict(text_test)))
    f.close()

def prepare_dataset():
   business_emails = extract_email_body(business_emails_path)
   personal_emails = extract_email_body(personal_emails_path)
   labeled_bemails = [('b', s) for s in business_emails]
   labeled_pemails = [('p', s) for s in personal_emails]
   sms_data = prepare_sms_data()
   dataset = labeled_bemails + labeled_pemails + sms_data
   random.shuffle(dataset)

   if not os.path.exists("dataset/"):
       os.makedirs("dataset/")
   with open(dataset_path, 'w') as f:
      writer = csv.writer(f, delimiter='\t')
      writer.writerows(dataset)
   f.close()

def prepare_sms_data():
    data = pd.read_csv(sms_data_path, sep='\t')
    interested_columns = ['text', 'category']
    #remove useless columns
    data = data[interested_columns]
    interested_categories = ['p', 'b']
    #remove rows where category is not personal or business
    data = data[data['category'].isin(interested_categories)]
    tuple_list = []
    for index, row in data.iterrows():
        tuple_list.append((row['category'], row['text'].translate(None, digits)))
    return tuple_list

def predict(text):
  svm_classifier = cPickle.load(open(classifier_path))
  svm_predict = svm_classifier.predict([text])[0]
  if(svm_predict == 'p'):
      print("personal")
  if(svm_predict == 'b'):
      print("business")

def main(path_to_text):
    text = ''
    with open(path_to_text, 'r') as f:
        lines = f.readlines()
        text = ''.join(lines)
    f.close()

    #if we have trained classifier
    if(os.path.isfile(classifier_path) != False):
        predict(text)
        sys.exit()

    #create dataset
    if(os.path.isfile(dataset_path) == False):
        prepare_dataset()

    #train classifier
    train_svm()

    #predict
    predict(text)

if __name__ == "__main__":
   main(sys.argv[1])
