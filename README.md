# Text classification

## Description
We have a binary classification task with labeled (business / personal) dataset. I used supervised learning algorithm - SVM (Support Vector Machine) because our classes are linearly separable. Also I did parameter tuning to search best parameters (trying linear / rbf kernel). 

Repository consists: 

- raw data from Enron mobile and subset of Enron email datasets (`data/`);
- processed data (`dataset/`)
- trained classifier (`classifier/`)
- report with the best classifier parameters and metrics result (`report/`)
- main script (`main.py`)
- detailed description of data exploration, cleaning and normalization (`description/report.pdf`)
- list of used libraries (`requirements.txt`)

## Program use

run `python main.py path_to_text_file`

If you want to process data and train classifier you should remove `dataset/` and `classifier/` folders.

## Metrics

Calculated for each category: 

- precision 
- recall
- f1-score 
- support

